import lazyLoading from './lazyLoading'

export default {
  name: 'Desempenho',
  meta: {
    icon: 'fa-table',
    expanded: false
  },

  children: [
    {
      acesso: false,
      name: 'Financeiro',
      tabela: 'relat_desemp_22e',
      path: '/desempenho/Financeiro',
      meta: {
        label: 'Financeiro'
      },
      component: lazyLoading('desempenho/Financeiro')
    },
    {
      acesso: false,
      name: 'Desempenho Gerencial Periodo',
      tabela: 'relat_desemp_250',
      path: '/desempenho/desempenhoGerencialPeriodo',
      meta: {
        label: 'Desempenho Gerencial Periodo'
      },
      component: lazyLoading('desempenho/DesempenhoGerencialPeriodo')
    },
    {
      acesso: false,
      name: 'DRE',
      tabela: 'relat_desemp_1ad',
      path: '/desempenho/demonstracaoResultadoExercicio',
      meta: {
        label: 'DRE'
      },
      component: lazyLoading('desempenho/DemonstracaoResultadoExercicio')
    }
  ]
}
