import lazyLoading from './lazyLoading'

export default {
  name: 'Arquivo',
  meta: {
    icon: 'fa-file-o',
    expanded: false
  },

  children: [
    {
      acesso: true,
      name: 'Itens',
      tabela: null,
      path: '/Produtos/busca',
      meta: {
        label: 'Lista de Item'
      },
      component: lazyLoading('itens/Itens')
    }
  ]
}
