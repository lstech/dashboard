/* eslint-disable */
import Vue from 'vue'
import Resource from 'vue-resource'
import NProgress from 'vue-nprogress'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
import router from './router'
import store from './store'
// import * as filters from './filters'
import { domain, count, prettyDate, dayWeek, ddmmyyyy, pluralize, currency, percent} from './filters'
import { TOGGLE_SIDEBAR } from 'vuex-store/mutation-types'
import config from './config'

// Import Install and register helper items
Vue.filter('count', count)
Vue.filter('domain', domain)
Vue.filter('prettyDate', prettyDate)
Vue.filter('dayWeek', dayWeek)
Vue.filter('ddmmyyyy', ddmmyyyy)
Vue.filter('pluralize', pluralize)
Vue.filter('currency', currency)
Vue.filter('percent', percent)

window._ = require('underscore')
window.swal = require('sweetalert')
Vue.use(Resource)
Vue.use(NProgress)

// Enable devtools
Vue.config.devtools = true

// Interceptor to add token to http requests
Vue.http.interceptors.push((request, next) => {
  var url = window.location.hostname
  var arrEmp = url.split('.')
  var usuario
  // modify request
  if (arrEmp[0] === 'www') {
    arrEmp.splice(0, 1)
  }

  request.headers.set('Accept', 'application/json')
  request.headers.set('Empresa', arrEmp[0])

  if (localStorage.getItem('usuario')) {
    usuario = JSON.parse(localStorage.usuario)
    request.headers.set('Authorization', 'Bearer ' + usuario.token)
    request.headers.set('FilialId', usuario.filial_id.toString())
  }
  // continue to next interceptor
  next((response) => {
    var url = router.currentRoute.fullPath
    if (url !== '/Login') {
      if (response.data.error === 'token_not_provided' || response.data.error === 'token_invalid' || response.data.error === 'token_expired') {
        window.localStorage.removeItem('usuario')
        window.localStorage.removeItem('acessos')
        router.push('/login')
      }
    }
  })
})

sync(store, router)

const nprogress = new NProgress({ parent: '.nprogress-container' })

const { state } = store

router.beforeEach((route, redirect, next) => {
  if (route.path !== '/login') {
    if (!localStorage.getItem('usuario')) {
      router.push('/login')
    }
  }
  if (state.app.device.isMobile && state.app.sidebar.opened) {
    store.commit(TOGGLE_SIDEBAR, false)
  }
  next()
})

// Object.keys(filters).forEach(key => {
//   Vue.filter(key, filters[key])
// })


const app = new Vue({
  router,
  store,
  nprogress,
  ...App
})

export { app, router, store }
