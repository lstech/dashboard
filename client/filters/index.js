const urlParser = document.createElement('a')

export function domain (url) {
  urlParser.href = url
  return urlParser.hostname
}

export function count (arr) {
  return arr.length
}

export function prettyDate (date) {
  var a = new Date(date)
  return a.toDateString()
}

export function ddmmyyyy (date) {
  if (date) {
    var d = new Date(date)
    var mes = d.getMonth()
    mes = (parseInt(mes)) + 1
    var dia = d.getDate() < 10 ? dia = '0' + d.getDate() : dia = d.getDate()
    mes = mes < 10 ? mes = '0' + mes : mes
    var ano = d.getFullYear()

    return dia + ' / ' + mes + ' / ' + ano
  } else {
    return ''
  }
}

export function dayWeek (date) {
  var d = new Date(date)

  var weekday = new Array(7)

  weekday[0] = 'Domingo'
  weekday[1] = 'Segunda'
  weekday[2] = 'Terça'
  weekday[3] = 'Quarta'
  weekday[4] = 'Quinta'
  weekday[5] = 'Sexta'
  weekday[6] = 'Sábado'

  return weekday[d.getDay()]
}

export function pluralize (time, label) {
  if (time === 1) {
    return time + label
  }

  return time + label + 's'
}

export function currency (value, localeString = 'pt-BR') {
  if (value.value) {
    localeString = value.attributes[0]
    value = value.value
  }

  value = Number(value)

  if (localeString === 'pt-BR') {
    return value.toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })
  }

  if (!localeString) {
    return value.toLocaleString('pt-BR', {
      currency: 'BRL',
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    })
  }
}

export function percent (v) {
  var value = Number(v)
  var num = value.toFixed(2)
  return num
}
