/* eslint-disable */
const API = 'https://api.dashboard.lsystems.inf.br/api/'
// const API = 'http://api.dashboard.dev/api/'
window._ = require('underscore')
window.moment = require('moment')

export default {
  API: API,
  configRoutes: function (acessos, routes) {
  	routes.forEach(function (route) {
	    _.map(acessos, function (value, index) {
	      if (route.tabela === index && value === true) {
	        route.acesso = true
	      }
	    })
	  })
  }
}
